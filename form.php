<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <title>WEB3</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
        <form action="" method="POST">
          ФИО:
          <label>
            <input name="name" placeholder="Введите ФИО">
          </label>
          <br>
          E-mail:
          <label>
            <input type="email" name="email" placeholder="Введите e-mail">
          </label><br /><br />
          <label>Дата рождения:
                <input name="data" value="2020-12-10" type="date"> </label><br>

          <p>Пол:</p>
          <label>
            <input type="radio" name="pol" checked="checked" value="M">М
          </label>
          <label>
            <input type="radio" name="pol" value="W" />Ж
          </label><br />

          <p>Количество конечностей</p>
          <label>
            <input type="radio" name="kon" value="0" />0
          </label>
          <label>
            <input type="radio" name="kon" value="1" />1
          </label>
          <label>
            <input type="radio" name="kon" value="2" />2
          </label>
          <label>
            <input type="radio" name="kon" value="3" />3
          </label>
          <label>
            <input type="radio" checked="checked" name="kon" value="4" />4
          </label>
         <br /><br />

           <p>Сверхспособности</p>
          <label>
            <select name="spos" multiple=multiple>
                <option value="immortality">Бессмертие</option>
                <option value="invisibility">Невидимость</option>
                <option value="levitation">Левитация</option>
            </select>
          </label><br /><br />

          <p id="os">Биография</p>
          <label>
            <textarea placeholder="Расскажите о себе" name="os" rows="8" cols="60"></textarea>
          </label>
          <br>

          <label>
            С контрактом ознакомлен:
            <input type="checkbox" name="checkbox">
          </label>
          <br>
          <input type="submit" value="Отправить">
        </form>
</body>
</html>
