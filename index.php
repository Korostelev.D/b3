<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }

  include('form.php');

  exit();
}

$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['email'])) {
  print('Заполните e-mail.<br/>');
  $errors = TRUE;
}
if (empty($_POST['os'])) {
  print('Заполните биографию.<br/>');
  $errors = TRUE;
}
if (empty($_POST['spos'])) {
    print('Выберите способность.<br/>');
    $errors = TRUE;
}
if (empty($_POST['checkbox'])) {
    print('Поставьте галочку.<br/>');
    $errors = TRUE;
}


if ($errors) {
  exit();
}


$user = 'u23971';
$pass = '3457564';
$db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


try {
  $str = implode(',',$_POST['spos']);
  
  $stmt = $db->prepare("INSERT INTO appl SET name = ?, email = ?, data = ?, kon = ?, os = ?");
  $stmt -> execute([$_POST['name'],$_POST['email'],$_POST['data'],$_POST['kon'],$_POST['os']]);

  $stmt = $db->prepare("INSERT INTO abilities SET abilities = ?");
  $stmt -> execute([$str]);
  
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');

